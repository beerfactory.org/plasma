defmodule Plasma.Repo.Migrations.CreatePrevEvents do
  use Ecto.Migration

  def change do
    create table(:prev_events, primary_key: false) do
      add :event_id, references(:events, type: :uuid)
      add :prev_event_id, references(:events, type: :uuid)
      timestamps()
    end
  end
end
