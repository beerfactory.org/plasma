defmodule Plasma.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events) do
      add :id, :uuid, primary_key: true
      add :event_id, :text
      add :state_key, :string
      add :origin, :string, null: false
      add :origin_server_ts, :timestamptz
      add :received_ts, :timestamptz
      add :type, :string, null: false
      add :sender_id, references(:users, type: :uuid)
      add :room_id, references(:rooms, type: :uuid)
      timestamps()
    end
  end
end
