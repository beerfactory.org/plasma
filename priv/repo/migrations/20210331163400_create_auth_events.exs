defmodule Plasma.Repo.Migrations.CreateAuthEvents do
  use Ecto.Migration

  def change do
    create table(:auth_events, primary_key: false) do
      add :event_id, references(:events, type: :uuid)
      add :auth_event_id, references(:events, type: :uuid)
      timestamps()
    end
  end
end
