defmodule Plasma.Repo.Migrations.CreateDevice do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :id, :uuid, primary_key: true
      add :device_id, :text, null: false
      add :display_name, :text
      add :access_token, :text
      add :account_id, references(:accounts, type: :uuid)
      add :last_seen, :utc_datetime
      timestamps()
    end
  end
end
