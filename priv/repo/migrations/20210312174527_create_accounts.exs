defmodule Plasma.Repo.Migrations.CreateAccount do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add :id, :uuid, primary_key: true
      add :user_id, references(:users, type: :uuid)
      add :password_hash, :text
      add :kind, :string
      timestamps()
    end
  end
end
