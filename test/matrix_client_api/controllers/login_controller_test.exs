defmodule Plasma.Matrix.ClientApi.Controllers.R0.LoginControllerTest do
  use Plasma.Matrix.ClientApi.ConnCase

  describe "Registration" do
    test "login with valid user/password succeed  with existing device", %{
      conn: conn
    } do
      # First register some user
      session = get_auth_session(conn)

      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => "someuser",
        "password" => "PaSSw0rd"
      }

      response =
        build_conn()
        |> put_req_header("content-type", "application/json")
        |> post(Routes.register_path(conn, :register), request)
        |> json_response(200)

      %{"device_id" => device_id} = response

      login_request = %{
        "identifier" => %{
          "type" => "m.id.user",
          "user" => "someuser"
        },
        "type" => "m.login.password",
        "password" => "PaSSw0rd",
        "device_id" => device_id
      }

      login_response =
        build_conn()
        |> post(Routes.login_path(conn, :login), login_request)
        |> json_response(200)

      assert %{
               "access_token" => _,
               "device_id" => response_device_id,
               "user_id" => _
             } = login_response

      assert device_id == response_device_id
    end

    test "login with valid user/password succeed without device", %{
      conn: conn
    } do
      # First register some user
      session = get_auth_session(conn)

      request = %{
        "auth" => %{"type" => "m.login.dummy", "session" => session},
        "username" => "someuser",
        "password" => "PaSSw0rd"
      }

      build_conn()
      |> put_req_header("content-type", "application/json")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

      login_request = %{
        "identifier" => %{
          "type" => "m.id.user",
          "user" => "someuser"
        },
        "type" => "m.login.password",
        "password" => "PaSSw0rd"
      }

      login_response =
        build_conn()
        |> post(Routes.login_path(conn, :login), login_request)
        |> json_response(200)

      assert %{
               "access_token" => token,
               "device_id" => _,
               "user_id" => _
             } = login_response

      assert not is_nil(token)
    end
  end
end
