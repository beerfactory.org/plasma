defmodule Plasma.Matrix.ClientApi.Controllers.R0.RoomControllerTest do
  use Plasma.Matrix.ClientApi.ConnCase
  import Plasma.Factory

  test "create room with unsupported room version", %{conn: conn} do
    device = insert(:device)

    request = %{"room_version" => "0"}

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> device.access_token)
      |> post(Routes.room_path(conn, :create_room), request)
      |> json_response(400)

    assert %{"errcode" => "M_UNSUPPORTED_ROOM_VERSION"} == response
  end

  test "create room with invalid visibility", %{conn: conn} do
    device = insert(:device)

    request = %{"visibility" => "transparent"}

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> device.access_token)
      |> post(Routes.room_path(conn, :create_room), request)
      |> json_response(400)

    assert Map.get(response, "errcode") == "M_BAD_JSON"
  end

  test "create room succeed", %{conn: conn} do
    device = insert(:device)

    request = %{}

    response =
      conn
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> device.access_token)
      |> post(Routes.room_path(conn, :create_room), request)
      |> json_response(200)

    assert %{"room_id" => _room_id} = response
  end
end
