defmodule Plasma.Matrix.ClientApi.Controllers.R0.AccountControllerTest do
  use Plasma.Matrix.ClientApi.ConnCase

  test "post /whoami", %{conn: conn} do
    # Register a test account
    session = get_auth_session(conn)

    request = %{
      "auth" => %{"type" => "m.login.dummy", "session" => session},
      "username" => "someuser",
      "password" => "PaSSw0rd"
    }

    register_response =
      build_conn()
      |> put_req_header("content-type", "application/json")
      |> post(Routes.register_path(conn, :register), request)
      |> json_response(200)

    assert %{"access_token" => access_token} = register_response

    # Test whoami
    response =
      build_conn()
      |> put_req_header("content-type", "application/json")
      |> put_req_header("authorization", "Bearer " <> access_token)
      |> post(Routes.account_path(conn, :whoami))
      |> json_response(200)

    assert %{"user_id" => _user_id} = response
  end
end
