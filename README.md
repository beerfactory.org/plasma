Plasma is open source *homeserver* implementation of the [Matrix protocol](https://matrix.org/docs/spec/).

[Matrix](https://matrix.org/) is an open standard for interoperable, decentralised, real-time communication over IP.
It can be used to power Instant Messaging, VoIP/WebRTC signalling, Internet of Things communication -
or anywhere you need a standard HTTP API for publishing and subscribing to data whilst tracking the
conversation history.

**THIS PROJECT IS STILL UNDER HEAVY CONSTRUCTION**

# Features

*This sections describes features planned to be implemented, not features currently available.
See [CHANGELOG](CHANGELOG.md) for current features status.*

Plasma implements server side of current [Matrix API specifications](https://matrix.org/docs/spec/) :
 - [client-server API](https://matrix.org/docs/spec/client_server/r0.6.1) (0.6.1)
 - [server-server API](https://matrix.org/docs/spec/server_server/unstable.html) (unstable)
 - [application service API](https://matrix.org/docs/spec/application_service/unstable.html) (unstable)
 - [identity service API](https://matrix.org/docs/spec/identity_service/unstable.html) (unstable)
 - [push gateway API](https://matrix.org/docs/spec/push_gateway/unstable.html) (unstable)


# Contributing

Contributions are welcome. Fill free to clone this repo, open issues, push PR, ...
You can also join the community on the project matrix room: [#plasma:beerfactory.org](https://matrix.to/#/!gaSksXUwCVFzPPqDNJ:beerfactory.org?via=beerfactory.org&via=matrix.org&via=poddery.com&via=raim.ist).

Plasma is written in [Elixir language](https://elixir-lang.org/) and relies on the Erlang VM which provides
a scalable platform.

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
