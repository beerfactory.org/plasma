defmodule Plasma.Matrix.ClientApi.Controllers.R0.RegisterController do
  use Plasma.Matrix.ClientApi, :controller
  alias Plug.Conn
  alias Plasma.RuntimeConfig
  alias Plasma.Users.Devices
  alias Plasma.Users.Accounts
  import Polyjuice.Util.ClientAPIErrors
  alias Plasma.Matrix.ClientApi.Controllers.Tools

  def register(conn, %{"kind" => kind}) when kind == "guest" do
    conn
    |> Conn.put_status(403)
    |> json(MForbidden.new("Guest registration not implemented"))
  end

  def register(conn, params) do
    with {:registration_enabled, true} <- RuntimeConfig.registration_enabled?() do
      ua = Tools.get_ua(conn)

      inhibit_login =
        case Map.get(params, "inhibit_login") do
          nil ->
            false

          value ->
            if(is_boolean(value)) do
              value
            else
              if String.upcase(value) == "TRUE", do: true, else: false
            end
        end

      registration_params = %{
        inhibit_login: inhibit_login,
        access_token:
          if(inhibit_login,
            do: nil,
            else: Devices.generate_access_token()
          ),
        kind: "user",
        device_id: Map.get(params, "device_id", Devices.generate_device_id()),
        display_name:
          Map.get(
            params,
            "initial_device_display_name",
            "#{ua} on #{ua.os}/#{ua.device}"
          ),
        password: Map.get(params, "password"),
        username: Map.get(params, "username")
      }

      case Plasma.Users.Accounts.register(registration_params) do
        {:ok, %{account: _account, user: user, device: device}} ->
          response =
            if inhibit_login do
              %{
                user_id: user.user_id,
                device_id: device.device_id
              }
            else
              %{
                user_id: user.user_id,
                device_id: device.device_id,
                access_token: device.access_token
              }
            end

          conn
          |> Conn.put_status(200)
          |> json(response)

        {:invalid_user_id} ->
          conn
          |> Conn.put_status(400)
          |> json(MInvalidUsername.new())

        {:user_id_already_exists} ->
          conn
          |> Conn.put_status(400)
          |> json(MUserInUse.new())

        {:error, _message} ->
          conn
          |> Conn.put_status(500)
          |> json(PlasmaInternalError.new())
      end
    else
      {:error, _message} ->
        conn
        |> Conn.put_status(500)
        |> json(PlasmaInternalError.new())

      {:registration_enabled, false} ->
        conn
        |> Conn.put_status(403)
        |> json(MForbidden.new("Registration is disabled"))
    end
  end

  def available(conn, %{"username" => username}) do
    with {:server_name, server_name} <- Plasma.RuntimeConfig.get_server_name(),
         {:ok, user_id} <-
           Polyjuice.Util.Identifiers.V1.UserIdentifier.new(
             username,
             server_name
           ) do
      case Accounts.get_account_by_user_id("#{user_id}") do
        nil -> conn |> json(%{"available" => true})
        _ -> conn |> Conn.put_status(400) |> json(MUserInUse.new())
      end
    else
      {:error, _} ->
        conn
        |> Conn.put_status(400)
        |> json(MInvalidUsername)
    end
  end

  def available(conn, _params) do
    conn
    |> Conn.put_status(400)
    |> json(MInvalidUsername.new())
  end
end
