defmodule Plasma.Matrix.ClientApi.Controllers.R0.AccountController do
  use Plasma.Matrix.ClientApi, :controller
  alias Plug.Conn

  def whoami(conn, _params) do
    with %{auth_device: auth_device} <- conn.assigns do
      conn
      |> Conn.put_status(200)
      |> json(%{"user_id" => auth_device.account.user.user_id})
    else
      _ ->
        conn
        |> Conn.put_status(500)
        |> json(Errors.PlasmaInternalError.new("Unexpected auth device"))
    end
  end
end
