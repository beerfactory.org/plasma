defmodule Plasma.Matrix.ClientApi.Controllers.R0.KeyManagementController do
  use Plasma.Matrix.ClientApi, :controller
  import Polyjuice.Util.ClientAPIErrors

  def query(conn, _params) do
    conn |> json(%{})
  end
end
