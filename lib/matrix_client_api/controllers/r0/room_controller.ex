defmodule Plasma.Matrix.ClientApi.Controllers.R0.RoomController do
  use Plasma.Matrix.ClientApi, :controller
  import Polyjuice.Util.ClientAPIErrors
  import Polyjuice.Util.ClientAPIErrors
  alias Plasma.Rooms
  alias Plug.Conn

  @default_visibility "private"
  @matrix_config Application.get_env(:plasma, :matrix)
  @supported_room_versions @matrix_config[:supported_room_versions]
  @default_room_version @matrix_config[:default_room_version]

  def create_room(conn, params) do
    room_version = Map.get(params, "room_version", @default_room_version)

    visibility = Map.get(params, "visibility", @default_visibility)

    with {:server_name, server_name} <- Plasma.RuntimeConfig.get_server_name(),
         %{auth_device: auth_device} <- conn.assigns,
         room_id <-
           Polyjuice.Util.Identifiers.V1.RoomIdentifier.generate(server_name),
         {:room_version, true} <-
           {:room_version, Enum.member?(@supported_room_versions, room_version)},
         {:room_visibility, true} <-
           {:room_visibility, Enum.member?(["private", "public"], visibility)},
         {:ok, room} <-
           Rooms.create_room("#{room_id}", room_version, visibility) do
      %{
        creator_id: auth_device.account.user.id,
        room_id: room.id,
        request: params
      }
      |> Plasma.Matrix.ClientApi.Workers.CreateRoomWorker.new()
      |> Oban.insert()

      conn |> json(%{"room_id" => room.room_id})
    else
      {:room_version, false} ->
        conn
        |> Conn.put_status(400)
        |> json(MUnsupportedRoomVersion.new())

      {:room_visibility, false} ->
        conn
        |> Conn.put_status(400)
        |> json(
          MBadJson.new("Invalid visibility parameter value '#{visibility}'")
        )

      {:error, message} ->
        conn
        |> Conn.put_status(500)
        |> json(Errors.PlasmaInternalError.new(message))
    end
  end
end
