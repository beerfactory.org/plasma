defmodule Plasma.Matrix.ClientApi.Controllers.R0.PresenceController do
  use Plasma.Matrix.ClientApi, :controller
  alias Plug.Conn
  require Logger
  import Polyjuice.Util.ClientAPIErrors
  alias Plasma.Presence

  @doc """
  Handles PUT requests to /_matrix/client/r0/presence/:userId/status
  """
  def update(
        conn,
        %{
          "userId" => user_id,
          "presence" => presence
        } = params
      ) do
    with %{auth_device: auth_device} <- conn.assigns do
      user = auth_device.account.user

      if user.user_id != user_id do
        conn
        |> Conn.put_status(403)
        |> json(MForbidden.new())
      else
        Presence.set_user_presence(
          user.user_id,
          presence,
          Map.get(params, "status_msg")
        )

        conn
        |> json(%{})
      end
    else
      _ ->
        conn
        |> Conn.put_status(500)
        |> json(Errors.PlasmaInternalError.new("Unexpected auth device"))
    end
  end

  def update(conn, _params) do
    conn
    |> Conn.put_status(400)
    |> json(MBadType.new())
  end

  @doc """
  Handles GET requests to /_matrix/client/r0/presence/:userId/status
  """
  def show(conn, %{"userId" => user_id}) do
    Logger.debug("presence requested for '#{user_id}'")

    with %{auth_device: auth_device} <- conn.assigns do
      user = auth_device.account.user
      state = Presence.get_user_presence(user.user_id)
      last_active_ago = DateTime.diff(DateTime.utc_now(), state.last_active)

      conn
      |> json(%{
        "presence" => state.presence,
        "status_msg" => state.status_msg,
        "last_active_ago" => last_active_ago
      })
    else
      :error ->
        conn
        |> Conn.put_status(404)
        |> json(MNotFound.new())
    end
  end
end
