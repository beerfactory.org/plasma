defmodule Plasma.Matrix.ClientApi.Controllers.Tools do
  def get_ua(conn) do
    conn
    |> Plug.Conn.get_req_header("user-agent")
    |> List.first()
    |> UAParser.parse()
  end
end
