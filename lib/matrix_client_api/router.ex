defmodule Plasma.Matrix.ClientApi.Router do
  use Plasma.Matrix.ClientApi, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  pipeline :auth do
    plug(Plasma.Matrix.ClientApi.Plugs.ExtractAccessToken)
    plug(Plasma.Matrix.ClientApi.Plugs.RequireAccessToken)
  end

  pipeline :interactive_auth do
    plug(Plasma.Matrix.ClientApi.Plugs.InteractiveAuthFlow)
  end

  scope "/_matrix/client", Plasma.Matrix.ClientApi.Controllers do
    pipe_through(:api)

    get("/versions", VersionsController, :get)

    scope "/r0", R0 do
      get("/login", LoginController, :get_login)
      post("/login", LoginController, :login)
      get("/register/available", RegisterController, :available)
    end
  end

  scope "/_matrix/client", Plasma.Matrix.ClientApi.Controllers do
    pipe_through([:api, :interactive_auth])

    scope "/r0", R0 do
      post("/register", RegisterController, :register)
    end
  end

  scope "/_matrix/client", Plasma.Matrix.ClientApi.Controllers do
    pipe_through([:api, :auth])

    scope "/r0", R0 do
      post("/account/whoami", AccountController, :whoami)
      post("/logout", LoginController, :logout)
      post("/logout/all", LoginController, :logout_all)
      get("/sync", SyncController, :sync)
      post("/createRoom", RoomController, :create_room)

      scope "/pushrules" do
        get("/", PushRulesController, :get)
      end

      scope "/keys" do
        post("/query", KeyManagementController, :query)
      end

      scope "/user/:user_id/filter" do
        post("/", FiltersController, :upload_filter)
        get("/:filter_id", FiltersController, :get_filter)
      end

      scope "/presence" do
        put "/:userId/status", PresenceController, :update
        get "/:userId/status", PresenceController, :show
      end
    end
  end
end
