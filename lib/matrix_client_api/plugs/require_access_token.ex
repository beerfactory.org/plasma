defmodule Plasma.Matrix.ClientApi.Plugs.RequireAccessToken do
  alias Plug.Conn
  import Phoenix.Controller, only: [json: 2]
  alias Plug.Conn
  import Polyjuice.Util.ClientAPIErrors

  @moduledoc """
  RequireAccessToken checks if a token (previously extracted by
  ExtractAccessToken plug) is present in `conn` assigns.
  If yes, the token presence in checked in known token table. Then `conn.assign`
  is completed with :
  - user_id which the token belongs to
  - device_id associated with the token
  Else, the connection is halted
  """

  def init(default), do: default

  def call(conn, _default) do
    case Map.fetch(conn.assigns, :access_token) do
      {:ok, token} ->
        credentials = Plasma.Users.Accounts.get_auth_device_with_account(token)

        if(credentials != nil) do
          Conn.assign(conn, :auth_device, credentials)
        else
          conn |> json(MUnknownToken) |> Conn.halt()
        end

      _ ->
        # Token is not found in conn assign then halt connection
        conn |> json(MMissingToken) |> Conn.halt()
    end
  end
end
