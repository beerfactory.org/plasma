defmodule Plasma.RoomServer.RoomServerState do
  @type t :: %__MODULE__{
          room: Plasma.Rooms.Room.t(),
          server_name: String.t()
        }

  @enforce_keys [:room, :server_name]
  defstruct [:room, :server_name]
end
