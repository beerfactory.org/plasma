defmodule Plasma.Cache do
  use Nebulex.Cache,
    otp_app: :plasma,
    adapter: Nebulex.Adapters.Multilevel

  defmodule L1 do
    use Nebulex.Cache,
      otp_app: :plasma,
      adapter: Nebulex.Adapters.Local
  end

  defmodule L2 do
    use Nebulex.Cache,
      otp_app: :plasma,
      adapter: Nebulex.Adapters.Partitioned
  end
end
