defmodule Plasma.Users do
  use Nebulex.Caching
  alias Plasma.Repo
  alias Plasma.Users.User

  @ttl :timer.hours(1)
  @decorate cacheable(
              cache: Plasma.Cache,
              key: {User, id},
              opts: [ttl: @ttl]
            )
  def get_user_by_id(id) do
    Repo.get(User, id)
  end
end
