defmodule Plasma.Users.Filter do
  use Plasma.Schema

  @type t :: %__MODULE__{
          definition: String.t(),
          account: Account.t()
        }

  schema "filters" do
    field :definition, :map
    belongs_to :account, Account
    timestamps()
  end

  def changeset(filter, attrs \\ %{}) do
    filter
    |> cast(attrs, [:definition])
    |> assoc_constraint(:account)
  end

  def create_changeset(filter, attrs \\ %{}), do: changeset(filter, attrs)
end
