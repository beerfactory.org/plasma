defmodule Plasma.Rooms.AuthEvent do
  use Plasma.Schema
  alias Plasma.Rooms.Event

  @primary_key false
  schema "auth_events" do
    belongs_to :event, Event
    belongs_to :auth_event, Event
    timestamps()
  end
end
