defmodule Plasma.Rooms.EventContent do
  use Plasma.Schema
  alias Plasma.Rooms.Event

  schema "event_contents" do
    field :content, :map
    belongs_to :event, Event
    timestamps()
  end

  def changeset(content, attrs \\ %{}) do
    content
    |> cast(attrs, [:content])
  end

  def create_changeset(content, attrs \\ %{}), do: changeset(content, attrs)
end
