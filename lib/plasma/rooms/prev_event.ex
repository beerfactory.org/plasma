defmodule Plasma.Rooms.PrevEvent do
  use Plasma.Schema
  alias Plasma.Rooms.Event

  @primary_key false
  schema "prev_events" do
    belongs_to :event, Event
    belongs_to :prev_event, Event
    timestamps()
  end
end
