defmodule Plasma.Rooms.Events do
  alias Ecto.Multi
  alias Plasma.Repo
  alias Plasma.Rooms.Room
  alias Plasma.Rooms.Event
  alias Plasma.Rooms.EventContent
  alias Plasma.Users.User
  alias Plasma.Rooms.PDU

  def create_event(
        params,
        %Room{} = room,
        %User{} = sender,
        prev_events,
        auth_events
      ) do
    res =
      insert_event_and_content(
        params,
        %Room{} = room,
        %User{} = sender,
        prev_events,
        auth_events
      )
      |> update_event_id(room)
      |> Repo.transaction()

    case res do
      {:ok, %{event: event, event_content: event_content}} ->
        {:ok, %{event: event, event_content: event_content}}

      {:error, :event, changeset, _} ->
        error_cause = inspect(Repo.changeset_fields_on_error(changeset))
        {:event_creation_failed, error_cause, changeset}

      other ->
        IO.inspect(other)
    end
  end

  defp insert_event_and_content(
         params,
         %Room{} = room,
         %User{} = sender,
         prev_events,
         auth_events
       ) do
    Multi.new()
    |> Multi.insert(
      :event,
      Event.create_changeset(
        %Event{},
        params,
        room,
        sender,
        prev_events,
        auth_events
      )
    )
    |> Multi.insert(:event_content, fn %{event: event} ->
      Ecto.build_assoc(event, :content)
      |> EventContent.create_changeset(params)
    end)
  end

  defp update_event_id(multi, %Room{} = room) do
    multi
    |> Multi.merge(fn %{event: event} ->
      Multi.new()
      |> Multi.run(:event_hash, fn repo, _ ->
        with full_event <-
               repo.get(Event, event.id)
               |> repo.preload([
                 :sender,
                 :room,
                 :content,
                 :prev_events,
                 :auth_events
               ]) do
          {:ok, generate_event_id(full_event, room.version)}
        end
      end)
      |> Multi.update(:event_id, fn %{event_hash: event_hash} ->
        Ecto.Changeset.change(event, event_id: event_hash)
      end)
    end)
  end

  defp generate_event_id(%Event{} = event, room_version) do
    with {:ok, event_map} <- PDU.event_to_pdu(event, room_version),
         {:ok, hash} <-
           Polyjuice.Util.Event.compute_content_hash(
             room_version,
             event_map
           ),
         {:server_name, server_name} <- Plasma.RuntimeConfig.get_server_name() do
      case room_version do
        "1" -> "$#{Base.encode64(hash, padding: false)}:#{server_name}"
        "2" -> "$#{Base.encode64(hash, padding: false)}:#{server_name}"
        "3" -> "$#{Base.encode64(hash, padding: false)}"
        "4" -> "$#{Base.url_encode64(hash, padding: false)}"
        "5" -> "$#{Base.url_encode64(hash, padding: false)}"
        "6" -> "$#{Base.url_encode64(hash, padding: false)}"
      end
    end
  end

  def get_event_by_id_with_preloads(id) do
    Repo.get(Event, id)
    |> Repo.preload([:sender, :room, :content, :prev_events, :auth_events])
  end
end
