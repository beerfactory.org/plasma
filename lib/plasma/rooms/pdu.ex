defmodule Plasma.Rooms.PDU do
  alias Plasma.Rooms.Event
  require Logger

  defmodule Module.concat(Plasma.Rooms.PDU.RoomVersion, "1") do
    @moduledoc false
    def event_to_pdu(event) do
      %{
        "auth_events" =>
          Enum.map(event.auth_events, fn event -> event.event_id end),
        "content" => event.content.content,
        "event_id" => event.event_id,
        # TODO
        "hashes" => %{sha256: ""},
        "origin" => event.origin,
        "origin_server_ts" =>
          DateTime.to_unix(event.origin_server_ts, :millisecond),
        "prev_events" =>
          Enum.map(event.prev_events, fn event -> event.event_id end),
        "redacts" => "",
        "room_id" => event.room.room_id,
        "sender" => event.sender.user_id,
        "signatures" => %{},
        "state_key" => event.state_key,
        "type" => event.type,
        "unsigned" => %{}
      }
    end
  end

  defmodule Module.concat(Plasma.Rooms.PDU.RoomVersion, "2") do
    @moduledoc false
    defdelegate event_to_pdu(event),
      to: Module.concat(Plasma.Rooms.PDU.RoomVersion, "1")
  end

  defmodule Module.concat(Plasma.Rooms.PDU.RoomVersion, "3") do
    @moduledoc false
    defdelegate event_to_pdu(event),
      to: Module.concat(Plasma.Rooms.PDU.RoomVersion, "1")
  end

  defmodule Module.concat(Plasma.Rooms.PDU.RoomVersion, "4") do
    @moduledoc false
    defdelegate event_to_pdu(event),
      to: Module.concat(Plasma.Rooms.PDU.RoomVersion, "1")
  end

  defmodule Module.concat(Plasma.Rooms.PDU.RoomVersion, "5") do
    @moduledoc false
    defdelegate event_to_pdu(event),
      to: Module.concat(Plasma.Rooms.PDU.RoomVersion, "1")
  end

  defmodule Module.concat(Plasma.Rooms.PDU.RoomVersion, "6") do
    @moduledoc false
    defdelegate event_to_pdu(event),
      to: Module.concat(Plasma.Rooms.PDU.RoomVersion, "1")
  end

  def event_to_pdu(%Event{} = event, room_version) do
    try do
      module = Module.safe_concat(Plasma.Rooms.PDU.RoomVersion, room_version)
      {:ok, apply(module, :event_to_pdu, [event])}
    rescue
      error ->
        Logger.error(error)
        :error
    end
  end
end
