defmodule Plasma.Rooms.EventTypes do
  def m_room_create, do: "m.room.create"
  def m_room_member, do: "m.room.member"
end
