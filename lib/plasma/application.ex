defmodule Plasma.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  alias Vapor.Provider.{Dotenv, File}
  require Logger

  def start(_type, _args) do
    # Init runtime defaults in case some runtime configuration are defaulted/commented
    runtimeDefaults = Application.get_env(:plasma, :runtime_defaults)[:config]

    # Load configuration from external YAML
    plasmaConfigPath =
      System.get_env("PLASMA_CONFIG_PATH", ".") <> "/plasma.yaml"

    Logger.info("Loading plasma from #{plasmaConfigPath}")

    providers = [
      %Dotenv{},
      %File{
        path: plasmaConfigPath,
        bindings: [
          {:database, "database"},
          {:enable_registration, "enable_registration"},
          {:server_name, "server_name"}
        ]
      }
    ]

    # If values could not be found we raise an exception and halt the boot process
    runtimeConfig = Map.merge(runtimeDefaults, Vapor.load!(providers))
    Logger.debug("Runtime configuration: #{inspect(runtimeConfig)}")
    Application.put_env(:plasma, :runtime_config, runtimeConfig)

    children = [
      Plasma.Cache,
      # Start the Ecto repository
      Plasma.Repo,
      # Start the Telemetry supervisor
      PlasmaWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Plasma.PubSub},
      {Oban, oban_config()},
      {Horde.Registry, [name: Plasma.Registry, keys: :unique]},
      {Horde.DynamicSupervisor,
       [name: Plasma.DistributedSupervisor, strategy: :one_for_one]},
      # Start Plasma main endpoint (http/https)
      PlasmaWeb.Endpoint,
      # Start Matrix client API endpoint (http/https)
      Plasma.Matrix.ClientApi.Endpoint
      # Start a worker by calling: Plasma.Worker.start_link(arg)
      # {Plasma.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Plasma.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PlasmaWeb.Endpoint.config_change(changed, removed)
    Plasma.Matrix.ClientApi.Endpoint.config_change(changed, removed)
    :ok
  end

  def get_runtime_config() do
    Application.fetch_env(:plasma, :runtime_config)
  end

  defp oban_config do
    Application.fetch_env!(:plasma, Oban)
  end
end
