defmodule Plasma.RuntimeConfig do
  def config_password_login_enabled?() do
    {:ok, %{password_config: config}} = Plasma.Application.get_runtime_config()
    String.downcase(config["enabled"]) == "true"
  end

  def registration_enabled?() do
    {:ok, %{enable_registration: enabled}} =
      Plasma.Application.get_runtime_config()

    {:registration_enabled, enabled}
  end

  def get_server_name() do
    case Plasma.Application.get_runtime_config() do
      {:ok, %{server_name: server_name}} -> {:server_name, server_name}
      _ -> {:error, "Can't find runtime configuration for server_name"}
    end
  end
end
