# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed
- implementation for `GET /_matrix/client/r0/presence/{userId}/status` client API endpoint ([r0.6.1](https://spec.matrix.org/unstable/client-server-api/#get_matrixclientr0presenceuseridstatus))
- implementation for `PUT /_matrix/client/r0/presence/{userId}/status` client API endpoint ([r0.6.1](https://spec.matrix.org/unstable/client-server-api/#put_matrixclientr0presenceuseridstatus))
- implementation for `POST /_matrix/client/r0/logout` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#post-matrix-client-r0-logout))
- implementation for `POST /_matrix/client/r0/logout/all` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#post-matrix-client-r0-logout-all))
- implementation for `GET /_matrix/client/r0/register/available` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#get-matrix-client-r0-register-available))
- implementation for `POST /_matrix/client/r0/login` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#post-matrix-client-r0-login))
- implementation for `POST /_matrix/client/r0/user/{userId}/filter` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#post-matrix-client-r0-user-userid-filter))
- implementation for `GET /_matrix/client/r0/user/{userId}/filter/{filterId}` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#get-matrix-client-r0-user-userid-filter-filterid))
- implementation for `POST /_matrix/client/r0/register` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#post-matrix-client-r0-register))
- implementation for `GET /_matrix/client/r0/account/whoami` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#get-matrix-client-r0-account-whoami))
- implementation for `GET /_matrix/client/versions` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#get-matrix-client-versions)) 
- implementation for `GET /.well-known/matrix/client` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#get-well-known-matrix-client)) 
- implementation for `GET /_matrix/client/r0/login` client API endpoint ([r0.6.1](https://matrix.org/docs/spec/client_server/r0.6.1#get-matrix-client-r0-login))
- Project resurection
